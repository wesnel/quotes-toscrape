{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Lib where

import           Control.Applicative
import qualified Data.Text.Lazy             as TL
import qualified Database.PostgreSQL.Simple as PS
import           GHC.Generics
import           Text.HTML.Scalpel

data Content = Content { quoteText   :: TL.Text
                       , quoteAuthor :: TL.Text }
                       deriving ( Show, Generic, PS.ToRow )

data Quote = Quote { content  :: Content
                   , tags     :: (Maybe [TL.Text]) }
                   deriving Show

data Page = Page { quotes :: [Quote]
                 , next   :: Maybe URL }
                 deriving Show

crawlQuotes :: Scraper TL.Text [Quote]
crawlQuotes = chroots ( "div" @: [hasClass "quote"] ) crawlQuote

crawlQuote :: Scraper TL.Text Quote
crawlQuote = do
    content <- do
        quoteText <- text $ "span" @: [hasClass "text"]
        quoteAuthor <- text $ "small" @: [hasClass "author"]
        return $ Content quoteText quoteAuthor
    tags <- chroot ( "meta" @: [hasClass "keywords"] ) $
        attr "content" anySelector
        >>= \ s -> case s of
                "" -> return Nothing
                _  -> (return . Just . TL.splitOn ",") s
    return $ Quote content tags

getNextPage :: Scraper TL.Text (Maybe URL)
getNextPage = let baseURL = "http://quotes.toscrape.com" in
    chroot ( "li" @: [hasClass "next"] ) $
        attr "href" "a"
        >>= return . Just
                   . TL.unpack
                   . TL.append baseURL

crawlPage :: URL -> IO ( Maybe Page )
crawlPage = flip scrapeURL $ do
    quotes <- crawlQuotes
    next <- getNextPage <|> return Nothing
    return $ Page quotes next
