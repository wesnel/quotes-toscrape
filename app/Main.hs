{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings   #-}

module Main where

import qualified Data.Text.Lazy             as TL
import qualified Database.PostgreSQL.Simple as PS
import           Lib
import           Text.HTML.Scalpel

main :: IO ()
main = do
    conn <- PS.connect
                PS.defaultConnectInfo
                    { PS.connectDatabase = "quotes-toscrape" }
    crawl conn "http://quotes.toscrape.com/"
    PS.close conn

crawl :: PS.Connection -> URL -> IO ()
crawl conn url = do
    page <- crawlPage url
    writePage page
    where
        writePage (Just (Page quotes Nothing))     = writeQuotes conn quotes
        writePage (Just (Page quotes (Just next))) = do
            writeQuotes conn quotes
            crawl conn next
        writePage _                                = return ()

writeQuotes :: PS.Connection -> [Quote] -> IO ()
writeQuotes conn = mapM_ $ writeQuote conn

writeQuote :: PS.Connection -> Quote -> IO ()
writeQuote conn (Quote content tags) = do
    let q = "insert into quotes (quoteText, quoteAuthor)\
            \ values (?, ?) returning quoteID"
    quoteID :: PS.Only Int <- PS.query conn q content >>= return . head
    case tags of
        Nothing -> return ()
        Just ts -> do
            let q' = "insert into tags (quoteID, quoteTag) values (?, ?)"
            PS.executeMany conn q' $
                zip (repeat $ PS.fromOnly quoteID) ts
            return ()
